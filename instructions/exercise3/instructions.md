
## Pre-requisites

## Deploy your add-on on Firebase

### Create a project
    Go to www.firebase.com and create an account
    Create an app through the firebase website

### Configure the build
    Open config.json and update the baseURL for the add-on in production
            (it should be <your_app_name>.firebaseapp.com)

### Build the add-on for the target environment
    bin/build-prod

### Deploy to firebase
    firebase init
    firebase deploy
    in the deploy command, specify the target directory as build/prod

### Create the listing
    Log in http://marketplace.atlassian.com and create a private listing
    Use the images from the folder "marketplace_assets"
    Make sure the addon AND the version are PRIVATE

### Install the add-on on a jira-dev instance
    Install the add-on
    Make sure you have enabled private marketplace listings in your instance's UPM
        (in the Manage Add-ons screen, at the bottom, Settings > Click enable private listings)