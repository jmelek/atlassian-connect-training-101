Windows: commands are run from bin/win
Linux: bin/linux


## Exercise #1
    Install the hello world add-on on a local JIRA or Confluence instance

## how to run the add-on

### Building the add-on
    Open config.json and change:
    addonKey (must be unique)
    addonName (make it unique if possible, e.g. with your initials)

    Running the build
    In a shell window, go to: bin
    run ./build-dev
    the files will be copied to build/dev

### Starting a web server to host the add-on
    In a shell window, go to: bin
    run ./start-http-server
    
    YOUR ADD-ON IS RUNNING!


## Some tips

### Validating a descriptor
    https://atlassian-connect-validator.herokuapp.com/validate
    

### Starting a local instance of the Atlassian Cloud product
    In a shell window, go to: bin
    Run ./start-jira
    JIRA is accessible at http://localhost:2990/jira
    Log in as admin/admin

### Installing an add-on
    Admin/Add-ons
    Manage add-ons
    Upload add-on 
    Paste the descriptor URL (http://localhost:8000/atlassian-connect.json)





