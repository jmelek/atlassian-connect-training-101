## Exercise #2
    Display "Hello <user name>" in helloyou.html, where <user name> is the display name of the logged in user


## Tips

### Accessing the REST API Browser (RAB)
    http://localhost:2990/jira/plugins/servlet/restbrowser

### Making a REST call to the Atlassian host using the Javascript API
    https://developer.atlassian.com/static/connect/docs/javascript/module-request.html

    
### Finding UI extension points
    Install this add-on to your JIRA instance
    https://marketplace.atlassian.com/plugins/com.wittified.webfragment-finder
