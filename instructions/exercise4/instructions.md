## Create a project

### Create a new project
    add the following as a new line to your local machine's /etc/hosts:
        127.0.0.1   precise32
    atlas-connect new <project_name>
    cd<project_name>
    npm install

### Start the add-on
    node app.js

### Open the ACE documentation
    https://bitbucket.org/atlassian/atlassian-connect-express

## Step 1
    display the user's name in the hello-world view, retrieving it using /rest/api/latest/myself


## Step 2
    Display the name of the user using the add-on in the hello-world view
    Note: you should use the URL parameter user_key

## Step 3
    Using webhooks, keep track of issues created by each user
    Add the list of issue keys created by the user viewing the the hello-world view.

## Step 4
    deploy the add-on in development mode on Heroku

## Some useful tools

### Decoding a JWT token
    http://jwt-decoder.herokuapp.com/jwt/decode
