module.exports = function (grunt) {

    var config = grunt.file.readJSON('config.json');
    var target = grunt.option('target') || 'dev';
    var product = grunt.option('product') || 'jira';
    var buildDir = 'build/' + config[target].targetDir + '/';
    var baseURL = config[target].baseURL;

    var addonKey = config.addonKey;
    var addonName = config.addonName;
    var pageLocation;
    var userEndpoint;
    console.log("Building for environment: " + target + " and product: " + product);
    if(product === 'confluence') {
        pageLocation = 'system.header/left';
        userEndpoint = '/rest/prototype/latest/user/current';
    } else {
        pageLocation = 'system.top.navigation.bar';
        userEndpoint = '/rest/api/latest/myself';
    }

    grunt.initConfig({
        replace: {
            addon: {
                src: ['src/*.*'],
                dest: buildDir,
                replacements: [
                    {
                        from: '{{baseURL}}',
                        to: baseURL
                    },
                    {
                        from: '{{addonKey}}',
                        to: addonKey
                    },
                    {
                        from: '{{addonName}}',
                        to: addonName
                    },
                    {
                        from: '{{pageLocation}}',
                        to: pageLocation
                    },
                    {
                        from: '{{userEndpoint}}',
                        to: userEndpoint
                    }
                ]
            }
        }
    });

    grunt.loadNpmTasks('grunt-text-replace');
    var target = grunt.option('target') || 'dev';
    grunt.registerTask('default', ['replace']);
};