#!/bin/sh
cd ../..
# Firebase bug, doesn't automatically create it and fails when running firebase deploy
rm -r -f /home/vagrant/tmp
mkdir /home/vagrant/tmp

grunt --target=prod --product=jira
